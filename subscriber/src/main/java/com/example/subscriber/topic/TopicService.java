package com.example.subscriber.topic;

import com.example.subscriber.user.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class TopicService {

    private final TopicRepository topicRepository;

    public List<String> getAllTopicsName() {
        Iterable<Topic> topicIterable = topicRepository.findAll();
        return StreamSupport.stream(topicIterable.spliterator(), true)
                .map(x -> x.getTopicName())
                .distinct()
                .collect(Collectors.toList());
    }

    public List<User> getAllUserByTopicName(String topicName) {
        List<Topic> topics = topicRepository.findAllByTopicName(topicName);
        List<User> users = new ArrayList<>();
        for (Topic topic : topics) {
            users.add(topic.getUser());
        }
        return users;
    }
}

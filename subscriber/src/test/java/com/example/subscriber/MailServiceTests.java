package com.example.subscriber;

import com.example.subscriber.mail.MailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;


@ActiveProfiles("test")
@SpringBootTest
class MailServiceTests {

    @Autowired
    JavaMailSender javaMailSender;

    MailService mailService;

    @BeforeEach
    void prepare() {
        mailService = new MailService(javaMailSender);
    }

    @Test
    void shouldThrowExceptionBecouseOfLackOfMonkey() {
        Throwable thrown = catchThrowable(() -> {
            mailService.sendMail("test", "test", "test", false);
        });

        assertThat(thrown).isInstanceOf(MailSendException.class);

    }

}

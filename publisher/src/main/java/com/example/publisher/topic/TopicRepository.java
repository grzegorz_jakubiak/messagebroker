package com.example.publisher.topic;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TopicRepository extends CrudRepository<Topic, Long> {
    @Query(value = "SELECT t FROM Topic t")
    List<Topic> findAll();
}

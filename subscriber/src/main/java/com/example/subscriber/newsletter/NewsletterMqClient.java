package com.example.subscriber.newsletter;

import com.example.subscriber.mail.MailService;
import com.example.subscriber.topic.TopicService;
import com.example.subscriber.user.User;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.List;

@Component
@AllArgsConstructor
@EnableAsync
public class NewsletterMqClient {

    private final MailService mailService;
    private final TopicService topicService;

    @RabbitListener(queues = "newsletter")
    public void newsletterListeners(Newsletter newsletter) {
        if (new Date().before(newsletter.getValidUntil())) {
            List<User> users = topicService.getAllUserByTopicName(newsletter.getTopicName());
            users.stream()
                    .forEach(user -> {
                        try {
                            mailService.sendMail(user.getEmail(), newsletter.getSubject(), newsletter.getContenValue(), false);
                        } catch (MessagingException e) {
                            e.printStackTrace();
                        }
                    });
        }
    }
}

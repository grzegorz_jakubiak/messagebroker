package com.example.subscriber;

import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class NewsletterGuiControllerE2ETests {


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private Flyway flyway;

    @BeforeEach
    void prepareDb() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    void shouldAddNewUserWithTopicsAndReturn201Accepted() {
        ResponseEntity<String> responseEntity = testRestTemplate.exchange("http://localhost:" + port + "/inputData?topicValues=java&topicValues=python&email=nowy1"
                , HttpMethod.POST
                , HttpEntity.EMPTY
                , String.class);
        Assertions.assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
    }

    @Test
    void shouldThrow404NotFound() {
        ResponseEntity<String> responseEntity = testRestTemplate.exchange("http://localhost:" + port + "/wrongPath"
                , HttpMethod.POST
                , HttpEntity.EMPTY
                , String.class);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @AfterEach
    void cleanDb() {
        flyway.clean();
        flyway.migrate();
    }
}

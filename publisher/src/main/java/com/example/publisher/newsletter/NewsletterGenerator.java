package com.example.publisher.newsletter;

import com.example.publisher.topic.Topic;
import com.example.publisher.topic.TopicService;
import com.example.publisher.user.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
class NewsletterGenerator {

    private final TopicService topicService;
    private final UserRepository userRepository;
    private final Random random;

    public NewsletterGenerator(TopicService topicService, UserRepository userRepository, @Value("#{new java.util.Random()}") Random random) {
        this.topicService = topicService;
        this.userRepository = userRepository;
        this.random = random;
    }

    @EventListener(ApplicationReadyEvent.class)
    List<Newsletter> generateNewsletterMessage() {
        List<Topic> topics = topicService.getAllTopics();

        return topics.stream()
                .map(x -> new Newsletter(String.valueOf(UUID.randomUUID())
                        , topics.get(random.nextInt(topics.size())).getTopicName()
                        , String.valueOf(UUID.randomUUID())
                        , new Date(new Date().getTime() + (1000 * 60 * 60 * 24))))
                .collect(Collectors.toList());
    }
}

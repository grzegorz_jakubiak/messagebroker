package com.example.publisher.newsletter;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class Newsletter {
    private String subject;
    private String topicName;
    private String contenValue;
    private Date validUntil;
}

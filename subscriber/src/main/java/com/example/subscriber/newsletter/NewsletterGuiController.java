package com.example.subscriber.newsletter;

import com.example.subscriber.topic.Topic;
import com.example.subscriber.topic.TopicService;
import com.example.subscriber.user.User;
import com.example.subscriber.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class NewsletterGuiController {
    private static final String SEARCH_TYPES = "topicTypes";

    private final TopicService topicService;
    private final UserRepository userRepository;

    @ModelAttribute
    public void initValues(Model model) {
        model.addAttribute(SEARCH_TYPES, topicService.getAllTopicsName());
    }

    @GetMapping("/newsletter")
    public String displayNewsletterTemplate() {
        return "newsletter";
    }

    @PostMapping("/inputData")
    public ResponseEntity<String> inputData(@RequestParam List<String> topicValues, @RequestParam String email) {
        Optional<User> userOptional = userRepository.findAllByEmail(email);
        User user = userOptional.orElseGet(()-> new User());
        user.setEmail(email);
        if(user.getTopicList()!=null){
            List<String> currentUserTopics = user.getTopicList().stream().map(x->x.getTopicName()).collect(Collectors.toList());
            topicValues.addAll(currentUserTopics);
        }

        List<Topic> topics = topicValues.stream().distinct().map(x -> new Topic(null, x, user)).collect(Collectors.toList());

        if(user.getTopicList()!=null){
            user.getTopicList().clear();
            user.getTopicList().addAll(topics);
        }

        User newUser = userRepository.save(user);
        return newUser!=null ? new ResponseEntity<>(HttpStatus.ACCEPTED) : new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

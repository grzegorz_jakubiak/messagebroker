package com.example.publisher.topic;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TopicService {

    private final TopicRepository topicRepository;

    public List<Topic> getAllTopics() {
        List<Topic> topics = topicRepository.findAll();
        return Optional.ofNullable(topics).orElseGet(() -> new ArrayList<>());
    }

}

package com.example.subscriber.newsletter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Newsletter {
    private String subject;
    private String topicName;
    private String contenValue;
    private Date validUntil;
}

create table user
(
    id bigint auto_increment
        primary key,
    email varchar(255) null
);

create table topic
(
    id bigint auto_increment
        primary key,
    topic_name varchar(255) null,
    user_id bigint null,
    constraint FK38wu074adxipuj6a9ifd7jv5f
        foreign key (user_id) references user (id)
);


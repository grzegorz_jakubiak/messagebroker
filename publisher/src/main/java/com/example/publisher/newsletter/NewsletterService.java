package com.example.publisher.newsletter;

import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class NewsletterService {

    private final RabbitTemplate rabbitTemplate;

    void pushNewsletterToQueue(List<Newsletter> newsletterList){
        if (newsletterList != null) {
            for (Newsletter newsletter : newsletterList) {
                rabbitTemplate.convertAndSend("newsletter", newsletter);
            }
        }
    }
}

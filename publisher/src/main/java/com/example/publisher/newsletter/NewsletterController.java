package com.example.publisher.newsletter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
class NewsletterController {

    private final NewsletterGenerator newsletterGenerator;
    private final AmqpAdmin amqpAdmin;
    private final NewsletterService newsletterService;

    @Bean
    public void configureQueue() {
        Queue queue = new Queue("newsletter", true, false, false);
        Binding binding = new Binding("newsletter", Binding.DestinationType.QUEUE, "", "newsletter", null);
        amqpAdmin.declareQueue(queue);
        amqpAdmin.declareBinding(binding);
    }

    @GetMapping("/processNewsletter")
    public void pushGeneratedNewsletter() {
        long start = System.currentTimeMillis();
        List<Newsletter> newsletterList = newsletterGenerator.generateNewsletterMessage();
        newsletterService.pushNewsletterToQueue(newsletterList);
        long elapsedTimeMillis = System.currentTimeMillis() - start;
        log.debug("Time method invocation: " + elapsedTimeMillis + " ms");
    }

    @PostMapping("/processNewsletter")
    public ResponseEntity<String> pushGeneratedNewsletter(@RequestBody Newsletter newsletter) {
        long start = System.currentTimeMillis();
        List<Newsletter> newsletterList = new ArrayList<>();
        newsletterList.add(newsletter);
        newsletterService.pushNewsletterToQueue(newsletterList);
        long elapsedTimeMillis = System.currentTimeMillis() - start;
        log.debug("Time method invocation: " + elapsedTimeMillis + " ms");
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
